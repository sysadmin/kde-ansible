<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// Secure the session cookie
ini_set('session.cookie_httponly', true);
ini_set('session.cookie_secure', {{ enable_ssl | bool | ternary('true','false') }});

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'KDE Identity',

	// preloading 'log' component
	'preload' => array('log', 'ldap'),

	// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.components.*',
		'application.validators.*',
		'ext.ldapsuite.*',
		'ext.yii-mail.*',
		'application.vendors.PEAR.*',
	),

	'modules' => array(),

	// application components
	'components' => array(
		'user' => array(
			'class' => 'WebUser',
			'autoUpdateFlash' => false,
		),
		'authManager' => array(
			'class' => 'PhpAuthManager',
		),
		'format' => array(
			'class' => 'application.components.Formatter',
			'dateFormat' => 'dd/MM/yyyy',
		),
		'request' => array(
			'enableCookieValidation' => true,
			'enableCsrfValidation' => true,
			'csrfCookie' => array('secure' => {{ enable_ssl | bool | ternary('true','false') }}, 'httpOnly' => true),
		),
		'securityManager' => array(
			'cryptAlgorithm' => 'rijndael-256',
		),
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		'ldap' => array(
			'class' => 'SLdapServer',
			'baseDn' => 'dc=kde,dc=org',
			'bindDn' => 'cn=solena-service,dc=kde,dc=org',
			'bindPassword' => '{{ldap_solena_service_password}}',
			'operateAsUser' => true,
		),
		'db' => array(
			'connectionString' => 'mysql:host=localhost;dbname=identity',
			'emulatePrepare' => true,
			'username' => 'identity',
			'password' => '{{mysql_user_password}}',
			'charset' => 'utf8',
		),
		'mail' => array(
			'class' => 'ext.yii-mail.YiiMail',
			'transportType' => 'php',
			'viewPath' => 'application.views.mail',
		),
		'tokenGrid' => array(
			'class' => 'application.components.TokenGridManager',
			'gridRows' => 10,
			'gridColumns' => 10,
			'tokenLength' => 4,
			'gridSalt' => '{{solena_gridsalt | default("")}}',
		),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),

	// application-level parameters
	'params' => array(
		'adminEmail' => 'identity-support@kde.org',
		'registerNotify' => 'sysadmin-systems@kde.org',
		'registrationUnit' => 'ou=people,dc=kde,dc=org',
		'defaultGroup' => 'users',
		'developerGroup' => 'developers',
		'disabledDeveloperGroup' => 'disabled-developers',
		'recaptcha-secret' => '{{recaptcha_secret}}',
		'recaptcha-sitekey' => '{{recaptcha_sitekey}}',
		'refererWhiteList' => array(
			'forum.kde.org',
			'projects.kde.org',
			'userbase.kde.org',
			'techbase.kde.org',
			'community.kde.org',
		),
	),
);
