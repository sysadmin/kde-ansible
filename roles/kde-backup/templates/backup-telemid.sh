LOCATION=/root/{{backup_directory}}

# Backup configuration and list of installed packages
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Transfer traditional backups to Hetzner Backup Space
lftp -f ~/bin/backup-options

# Prepare to run Borg backups
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'

{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
BORG_SERVER="ssh://{{backup_creds.username}}@{{backup_creds.hostname}}:23"

# Backup Homes
export BORG_REPO="$BORG_SERVER/./borg-backups/homes"
borg create --compression none --exclude-caches --exclude /home/akademy/2017 --exclude /home/marble/newstuff-tmp ::'{hostname}-homes-{now}' /home/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

# Backup Maps
export BORG_REPO="$BORG_SERVER/./borg-backups/maps"
borg create --compression none --exclude-caches ::'{hostname}-maps-{now}' /srv/www/maps.kde.org/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"
