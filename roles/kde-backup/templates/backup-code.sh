#!/bin/bash
LOCATION=/root/{{backup_directory}}

# Cleanup old backups
find $LOCATION -name '*z' -mtime +21 | xargs rm -f

# Grab the private sysadmin and board repos
tar -cJf $LOCATION/evboard-repo.`date +%w`.tar.xz -C / srv/evboard/

# Grab the gitolite-admin repositories, which are private, for git.neon.kde.org
tar -cJf $LOCATION/gitolite-admin-neon.`date +%w`.tar.xz -C / srv/neon/gitolite-admin.git/

# Grab the system config, package listing and cronjobs
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Secure our backups
chmod -R 700 $LOCATION

# Transfer them to the backup server
cd $LOCATION/..
lftp -f ~/bin/backup-options

# Prepare to run Borg backups
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'

{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
BORG_SERVER="ssh://{{backup_creds.username}}@{{backup_creds.hostname}}:23"

# Backup Homes
export BORG_REPO="$BORG_SERVER/./borg-backups/code-homes"
borg create --compression zlib,5 --exclude-caches ::'{hostname}-phabricator-{now}' /home/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

# Backup Databases
export BORG_REPO="$BORG_SERVER/./borg-backups/db-backups"
su -l phabricator -c "cd /srv/phabricator/phabricator && ./bin/storage dump --no-indexes" | borg create --compression lzma,3 ::'{hostname}-db-backups-{now}' - 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

# Backup Phabricator
export BORG_REPO="$BORG_SERVER/./borg-backups/phabricator"
borg create --compression zlib,5 --exclude-caches --exclude /srv/phabricator/logs/ --exclude /srv/phabricator/repos/ ::'{hostname}-phabricator-{now}' /srv/phabricator/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"
