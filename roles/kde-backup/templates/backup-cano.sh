LOCATION=/root/{{backup_directory}}

# Cleanup old backups
find $LOCATION -name '*gz' -mtime +7 | xargs rm -f

# Backup system configuration, crontabs and package listing
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Secure the backups
chmod -R 700 $LOCATION

# Transfer them to their final home
cd $LOCATION/..
{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
rsync --timeout=600 --delete -a -e 'ssh -p23' {{backup_directory}}/ {{backup_creds.username}}@{{backup_creds.hostname}}:cano-backups/
