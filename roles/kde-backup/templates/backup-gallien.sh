#!/bin/bash
LOCATION=/root/{{backup_directory}}

# Cleanup old backups
find $LOCATION -name '*z' -mtime +21 | xargs rm -f

# Grab the system config, package listing and cronjobs
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Secure our backups
chmod -R 700 $LOCATION

# Transfer them to the backup server
cd $LOCATION/..
lftp -f ~/bin/backup-options

# Prepare to run Borg backups
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'

{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
BORG_SERVER="ssh://{{backup_creds.username}}@{{backup_creds.hostname}}:23"

# Backup databases
export BORG_REPO="$BORG_SERVER/./borg-backups/gallien-databases"
for DATABASE in mykde; do
    mysqldump --opt --quick --single-transaction --skip-extended-insert --events --create-options --set-charset $DATABASE | borg create --compression lzma,3 "::{hostname}-$DATABASE-{now}" - 2>&1 | grep -v "Compacting segments"
    borg prune --prefix "{hostname}-$DATABASE" --keep-daily 7 --keep-weekly 4 --keep-monthly 3 2>&1 | grep -v "Compacting segments"
done

# Backup /srv
export BORG_REPO="$BORG_SERVER/./borg-backups/gallien-srv"
borg create --compression zlib,5 --exclude-caches ::'{hostname}-srv-{now}' /srv/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 3 2>&1 | grep -v "Compacting segments"
