#!/bin/bash
LOCATION=/root/{{backup_directory}}

# Backup configuration and list of installed packages
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Grab Grafana's own backups
cp /var/lib/grafana/grafana.db $LOCATION/grafana.db.`date +%w`

# Transfer backups
chmod -R 700 $LOCATION
{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
rsync --timeout=600 --delete -a -e 'ssh -p23' {{backup_directory}}/ {{backup_creds.username}}@{{backup_creds.hostname}}:backups/

# Prepare to run Borg
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'
BORG_SERVER="ssh://{{backup_creds.username}}@{{backup_creds.hostname}}:23"

# Backup InfluxDB for Grafana
export BORG_REPO="$BORG_SERVER/./borg-backups/influxdb-grafana"
borg create --compression zlib,3 --exclude-caches ::'{hostname}-influxdb-{now}' /var/lib/influxdb 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"
