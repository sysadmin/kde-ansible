---
- name: ensure bin directory exists
  file:
    path: /root/bin
    state: directory
    owner: root
    group: root

- name: ensure backup directory exists
  file:
    path: /root/{{backup_directory}}
    state: directory
    owner: root
    group: root
    mode: 0700

- name: install Hetzner ftp script
  template:
    src: backup-options.j2
    dest: /root/bin/backup-options
    owner: root
    group: root
    mode: 0600
  when: hetzner_backup_host is defined and 'lftp' in backup_apt_dependencies

- name: add Hetzner's host key to known_hosts (port 22)
  lineinfile:
    path: /root/.ssh/known_hosts
    line: "{{hetzner_backup_creds[hetzner_backup_host].hostname}} ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA5EB5p/5Hp3hGW1oHok+PIOH9Pbn7cnUiGmUEBrCVjnAw+HrKyN8bYVV0dIGllswYXwkG/+bgiBlE6IVIBAq+JwVWu1Sss3KarHY3OvFJUXZoZyRRg/Gc/+LRCE7lyKpwWQ70dbelGRyyJFH36eNv6ySXoUYtGkwlU5IVaHPApOxe4LHPZa/qhSRbPo2hwoh0orCtgejRebNtW5nlx00DNFgsvn8Svz2cIYLxsPVzKgUxs8Zxsxgn+Q/UvR7uq4AbAhyBMLxv7DjJ1pc7PJocuTno2Rw9uMZi1gkjbnmiOh6TTXIEWbnroyIhwc8555uto9melEUmWNQ+C+PwAK+MPw=="
  when: hetzner_backup_host is defined

- name: add Hetzner's host key to known_hosts (port 23)
  lineinfile:
    path: /root/.ssh/known_hosts
    line: "[{{hetzner_backup_creds[hetzner_backup_host].hostname}}]:23 ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIICf9svRenC/PLKIL9nk6K/pxQgoiFC41wTNvoIncOxs"
  when: hetzner_backup_host is defined

- name: ensure host has a ssh key
  user:
    name: root
    generate_ssh_key: yes
  register: root_user
  when: gohma_backup_user is defined

- name: ensure a backup account exists in micrea
  delegate_to: micrea.kde.org
  user:
    name: "{{gohma_backup_user}}"
    home: "/home/{{gohma_backup_home}}/"
    createhome: no
  when: gohma_backup_user is defined

- name: create micrea home directory
  delegate_to: micrea.kde.org
  file:
    state: directory
    dest: "/home/{{gohma_backup_home}}"
    owner: root
    group: root
    mode: 0755
  when: gohma_backup_user is defined

- name: create backup directory on micrea
  delegate_to: micrea.kde.org
  file:
    state: directory
    dest: "/home/{{gohma_backup_home}}/{{backup_directory}}"
    owner: "{{gohma_backup_user}}"
    group: "{{gohma_backup_user}}"
    mode: 0700
  when: gohma_backup_user is defined

- name: create .ssh in micrea account
  delegate_to: micrea.kde.org
  file:
    state: directory
    dest: "/home/{{gohma_backup_home}}/.ssh"
    owner: root
    group: root
    mode: 0755
  when: gohma_backup_user is defined

- name: assign authorized keys
  delegate_to: micrea.kde.org
  copy:
    dest: "/home/{{gohma_backup_home}}/.ssh/authorized_keys"
    owner: root
    group: root
    mode: 0644
    content: "no-pty,no-x11-forwarding,no-port-forwarding,no-agent-forwarding {{root_user.ssh_public_key}}\n"
  when: gohma_backup_user is defined

# This will *fail* if micrea is not present in known_hosts,
# and will need manual intervention to add it.
# A better solution might need a custom Ansible module.
- name: ensure micrea is in known_hosts
  command: ssh-keygen -F micrea.kde.org
  check_mode: no
  changed_when: False
  when: gohma_backup_user is defined

# Latest lftp in Bionic is buggy (launchpad bug 1904601)
- name: select older lftp version on Ubuntu Bionic
  set_fact:
    backup_apt_dependencies: "{{backup_apt_dependencies | map('regex_replace', '^lftp$', 'lftp=4.8.1-1ubuntu0.1') | list}}"
  when: "'lftp' in backup_apt_dependencies and ansible_distribution_release == 'bionic'"

- name: install dependencies of backup script
  apt:
    name: "{{backup_apt_dependencies}}"
    state: present

- name: install backup script
  template:
    src: backup-{{inventory_hostname_short}}.sh
    dest: /root/bin/run-backup.sh
    owner: root
    group: root
    mode: 0740

- name: ensure cronjob messages are emailed to sysadmin
  cron:
    user: root
    env: yes
    name: MAILTO
    value: sysadmin-systems@kde.org

- name: configure backup cronjob
  cron:
    hour: "{{backup_cron.hour}}"
    minute: "{{backup_cron.minute}}"
    weekday: "{{backup_cron.weekday | default('*')}}"
    user: root
    job: /root/bin/run-backup.sh
    name: "run backup"


- block:
  - name: install Hetzner ftp script for gitolite logs
    template:
      src: backup-options-logs.j2
      dest: /root/bin/backup-options-logs
      owner: root
      group: root
      mode: 0600

  - name: configure cronjob for gitolite log backups
    cron:
      hour: "{{backup_logs_cron.hour}}"
      minute: "{{backup_logs_cron.minute}}"
      weekday: "{{backup_logs_cron.weekday | default('*')}}"
      user: root
      job: "lftp -f ~/bin/backup-options-logs"
      name: "backup gitolite logs"

  when: inventory_hostname == 'code.kde.org'
