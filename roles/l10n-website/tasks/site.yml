---
- name: install packages required by ansible
  apt:
    name: python3-pymysql

- name: create various base directories
  file:
    path: "{{ item }}"
    state: directory
    owner: "{{ l10nuser }}"
    group: "{{ l10nuser }}"
    mode: 0755
  loop:
    - "{{ l10nsite_basedir }}"
    - "{{ l10nwebsite_dir }}"
    - "{{ pooverview_dir }}"
    - "{{ l10ncheckout_dir }}"

- name: checkout the subversion site
  subversion:
    repo: "{{ l10nsite_repo }}"
    dest: "{{ l10nwebsite_dir }}"

- name: checkout the capacity repository
  git:
    repo: "{{ capacity_repo }}"
    dest: "{{ capacity_dir }}"

- name: checkout the pology repository
  git:
    repo: "{{ pology_repo }}"
    dest: "{{ pology_dir }}"

- name: fix the permissions of the pology directory
  file:
    path: "{{ pology_dir }}"
    recurse: yes
    owner: "{{ l10nuser }}"
    group: "{{ l10nuser }}"

- name: create the website config file
  template:
    src: etc_l10n.kde.org.conf.j2
    dest: /etc/l10n.kde.org.conf
    owner: www-data
    group: www-data
    mode: 0640

- name: create the website config file for statistic generation
  template:
    src: _l10n-stats.conf.j2
    dest: "{{ l10nuser_dir }}/.l10n-stats.conf"
    owner: "{{ l10nuser }}"
    group: "{{ l10nuser }}"
    mode: 0640

- name: initialize the mysql database for the website
  mysql_db:
    login_host: "{{ l10nsite_db_host|default(omit) }}"
    login_unix_socket: "{{ l10nsite_db_socket|default(omit) }}"
    name: "{{ l10nsite_db_name }}"
    state: present
  # an handler makes sure the import operation is only executed
  # when the db is create, not when the task is executed again.
  notify:
    - import the l10n db

- name: initialize the mysql user for the website
  mysql_user:
    host: "{{ l10nsite_db_host|default(omit) }}"
    login_unix_socket: "{{ l10nsite_db_socket|default(omit) }}"
    name: "{{ l10nsite_db_user }}"
    password: "{{ l10nsite_db_pass }}"
    priv: '{{ l10nsite_db_name }}.*:ALL'
    state: present

# this is needed for mysql 8
- name: configure the mysql compatibility authentication
  copy:
    src: mysql_auth_compat.cnf
    dest: /etc/mysql/conf.d/rosetta-auth-compat.cnf
  notify: reload mysql

- name: configure the main l10n virtualhost
  template:
    src: l10n.kde.org.conf.j2
    dest: /etc/apache2/sites-available/l10n.kde.org.conf
  notify: reload Apache

- name: configure the www-redirect virtualhost
  copy:
    src: www.l10n.kde.org.conf
    dest: /etc/apache2/sites-available/www.l10n.kde.org.conf
  notify: reload Apache

- name: configure the l10n virtualhosts
  template:
    src: l10n_lang_vhosts.conf.j2
    dest: /etc/apache2/sites-available/{{ item.key }}.l10n.kde.org.conf
  loop: "{{ l10nsite_l10n_vhosts|dict2items }}"
  notify: reload Apache

- name: enable the l10n and the www virtualhosts
  file:
    state: link
    src: ../sites-available/{{ item }}.l10n.kde.org.conf
    dest: /etc/apache2/sites-enabled/{{ item }}.l10n.kde.org.conf
  loop: "{{ l10nsite_l10n_vhosts|dict2items|map(attribute='key')|list + ['www'] }}"
  notify: reload Apache

- name: enable the main virtualhost
  file:
    state: link
    src: ../sites-available/l10n.kde.org.conf
    dest: /etc/apache2/sites-enabled/l10n.kde.org.conf
  notify: reload Apache
